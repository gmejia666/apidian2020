# **API REST DIAN 2 - UBL 2.1 - VALIDACIÓN PREVIA**

## Acerca de FACTURALATAM - ACTUALIZACION

Estamos enfocados en crear herramientas y soluciones para desarrolladores de software, empresas medianas y pequeñas, tenemos un sueño, queremos democratizar la facturación electrónica.

## Antes de empezar...

### Preguntas Frecuentes [Aquí](https://drive.google.com/file/d/1RvneR6LaAjy98R4phKEzETiwU7y5WItv/view?usp=sharing)

### Campos principales del Anexo técnico de Facturación Electrónica [Aquí](https://drive.google.com/file/d/1fXKQ9Rw6GCgOsu1dqEqaruVabg5VxeUT/view?usp=sharing)

### Proceso de habilitación DIAN Validación Previa UBL 2.1 - Obtener parámetros para software directo [Aquí](https://youtu.be/0ThLJBzWyUA)


## API REST - Instalación

Para conocer el proceso de instalación de la api:

* Instalacion bajo entorno Windows: (https://drive.google.com/file/d/1gaGLi8xcm8wHlqT-qzAYD00_AZJRmpcW/view?usp=sharing "Clic")
* Instalacion bajo entorno Linux: (https://drive.google.com/file/d/1xxca_QkLtS1xlh5Rd9QFcDEJKmoIrfgI/view?usp=sharing "Clic")
* Instalacion bajo entorno Google VPS: (https://drive.google.com/file/d/1KcVyhhyFhnr-fzIX9gCUX9OT1CqfAyMB/view?usp=sharing "Clic")
* Instalacion bajo un solo entorno Hentzner: (https://docs.google.com/document/d/1hz3W9dn7HJJD9NSfo6zc4jjidQoPtSJZmoljUbqIy94/edit "Clic")

## Ejemplos para la API

 Primero descargue la herramienta POSTMAN desde internet, y luego importe el archivo collection aquí debajo.

## Collection POSTMAN

Puede descargar el [postman collection](https://drive.google.com/file/d/1qdr4oBxZWqwZSRErM6uQL_6wn9wt8WKm/view?usp=sharing "Clic") para que realice las pruebas
 
## Videos para la instalación y despliegue 
* Factura electrónica Colombia DIAN validación previa - PHP (1/4) Introducción [Aquí](https://www.youtube.com/watch?v=FwBaQfI2ghw)
* Factura electrónica Colombia DIAN validación previa - PHP (2/4) Instalación [Aquí](https://youtu.be/-DMnOKSaWr8)
* Factura electrónica Colombia DIAN validación previa - PHP (3/4) Configuración API [Aquí](https://youtu.be/CAyZ9suZLII)
* Factura electrónica Colombia DIAN validación previa - PHP (4/4) Envío Factura [Aquí](https://youtu.be/28l74DvL8_o)
* Video que demuestra funcionalidad de firma, envio y consulta de estado de documentos XML [Aqui](https://drive.google.com/open?id=15-saHPT-NsWZEWrNMciteSU9bBGh_Msf)





