<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Traits\DocumentTrait;
use App\Document;
use App\Customer;
use App\Company;

class InvoiceMail extends Mailable
{
    use DocumentTrait, Queueable, SerializesModels;

    public $invoice;
    public $customer;
    public $company;
    public $GuardarEn;
    public $PDFAlternativo;
    public $filename;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invoice,  $customer,  $company, $GuardarEn = FALSE, $PDFAlternativo = FALSE, $filename = FALSE)
    {
        $this->invoice = $invoice;
        $this->customer = $customer;
        $this->company  = $company;
        $this->filename = $filename;
        if($PDFAlternativo){
            $this->PDFAlternativo = TRUE;
            if($GuardarEn)
                $file = fopen($GuardarEn."\\PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf", "w");
            else
                $file = fopen(storage_path("app/public/{$this->company->identification_number}/PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf"), "w");
            fwrite($file, base64_decode($PDFAlternativo));
            fclose($file);
        }
        else
            $this->PDFAlternativo = FALSE;
    }

    public function build()
    {
        if($this->GuardarEn)
            if($this->PDFAlternativo)
                if(env('MAIL_USERNAME')){
                    if($this->filename)
                        $nameZIP = $this->zipEmail($this->GuardarEn."\\{$this->filename}.xml", $this->GuardarEn."\\PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf");
                    else
                        $nameZIP = $this->zipEmail($this->GuardarEn."\\{$this->filename}.xml", $this->GuardarEn."\\PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf");
                    return $this->view('mails.mail')->subject("Emitido por {$this->company->identification_number} - {$this->company->user->name} - Comprobante N⁰ {$this->invoice[0]->prefix}{$this->invoice[0]->number} Tipo Documento: {$this->invoice[0]->type_document->code}")
                                                    ->from(env('MAIL_USERNAME'))
                                                    ->attach($nameZIP);
                }
                else{
                    if($this->filename)
                        $nameZIP = $this->zipEmail($this->GuardarEn."\\{$this->filename}.xml", $this->GuardarEn."\\PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf");
                    else
                        $nameZIP = $this->zipEmail($this->GuardarEn."\\{$this->filename}.xml", $this->GuardarEn."\\PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf");
                    return $this->view('mails.mail')->subject("Emitido por {$this->company->identification_number} - {$this->company->user->name} - Comprobante N⁰ {$this->invoice[0]->prefix}{$this->invoice[0]->number} Tipo Documento: {$this->invoice[0]->type_document->code}")
                                                    ->from(config('mail.username'))
                                                    ->attach($nameZIP);
                }
            else
                if(env('MAIL_USERNAME')){
                    if($this->filename)
                        $nameZIP = $this->zipEmail($this->GuardarEn."\\{$this->filename}.xml", $this->GuardarEn."\\".$this->invoice[0]->pdf);
                    else
                        $nameZIP = $this->zipEmail($this->GuardarEn."\\{$this->filename}.xml", $this->GuardarEn."\\".$this->invoice[0]->pdf);
                    return $this->view('mails.mail')->subject("Emitido por {$this->company->identification_number} - {$this->company->user->name} - Comprobante N⁰ {$this->invoice[0]->prefix}{$this->invoice[0]->number} Tipo Documento: {$this->invoice[0]->type_document->code}")
                                                    ->from(env('MAIL_USERNAME'))
                                                    ->attach($nameZIP);
                }
                else{
                    if($this->filename)
                        $nameZIP = $this->zipEmail($this->GuardarEn."\\{$this->filename}.xml", $this->GuardarEn."\\FES-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf");
                    else
                        $nameZIP = $this->zipEmail($this->GuardarEn."\\{$this->filename}.xml", $this->GuardarEn."\\FES-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf");
                    return $this->view('mails.mail')->subject("Emitido por {$this->company->identification_number} - {$this->company->user->name} - Comprobante N⁰ {$this->invoice[0]->prefix}{$this->invoice[0]->number} Tipo Documento: {$this->invoice[0]->type_document->code}")
                                                    ->from(config('mail.username'))
                                                    ->attach($nameZIP);
                }
        else
            if($this->PDFAlternativo)
                if(env('MAIL_USERNAME')){
                    if($this->filename)
                        $nameZIP = $this->zipEmail(storage_path("app/public/{$this->company->identification_number}/{$this->filename}.xml"), storage_path("app/public/{$this->company->identification_number}/PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf"));
                    else
                        $nameZIP = $this->zipEmail(storage_path("app/public/{$this->company->identification_number}/{$this->filename}.xml"), storage_path("app/public/{$this->company->identification_number}/PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf"));
                    return $this->view('mails.mail')->subject("Emitido por {$this->company->identification_number} - {$this->company->user->name} - Comprobante N⁰ {$this->invoice[0]->prefix}{$this->invoice[0]->number} Tipo Documento: {$this->invoice[0]->type_document->code}")
                                                    ->from(env('MAIL_USERNAME'))
                                                    ->attach($nameZIP);
                }
                else{
                    if($this->filename)
                        $nameZIP = $this->zipEmail(storage_path("app/public/{$this->company->identification_number}/{$this->filename}.xml"), storage_path("app/public/{$this->company->identification_number}/PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf"));
                    else
                        $nameZIP = $this->zipEmail(storage_path("app/public/{$this->company->identification_number}/{$this->filename}.xml"), storage_path("app/public/{$this->company->identification_number}/PDF-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf"));
                    return $this->view('mails.mail')->subject("Emitido por {$this->company->identification_number} - {$this->company->user->name} - Comprobante N⁰ {$this->invoice[0]->prefix}{$this->invoice[0]->number} Tipo Documento: {$this->invoice[0]->type_document->code}")
                                                    ->from(config('mail.username'))
                                                    ->attach($nameZIP);
                }
            else{
                if(env('MAIL_USERNAME')){
                    if($this->filename)
                        $nameZIP = $this->zipEmail(storage_path("app/public/{$this->company->identification_number}/{$this->filename}.xml"), storage_path("app/public/{$this->company->identification_number}/{$this->invoice[0]->pdf}"));
                    else
                        $nameZIP = $this->zipEmail(storage_path("app/public/{$this->company->identification_number}/{$this->filename}.xml"), storage_path("app/public/{$this->company->identification_number}/{$this->invoice[0]->pdf}"));
                    return $this->view('mails.mail')->subject("Emitido por {$this->company->identification_number} - {$this->company->user->name} - Comprobante N⁰ {$this->invoice[0]->prefix}{$this->invoice[0]->number} Tipo Documento: {$this->invoice[0]->type_document->code}")
                                                    ->from(env('MAIL_USERNAME'))
                                                    ->attach($nameZIP);
                }
                else{
                    if($this->filename)
                        $nameZIP = $this->zipEmail(storage_path("app/public/{$this->company->identification_number}/{$this->filename}.xml"), storage_path("app/public/{$this->company->identification_number}/FES-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf"));
                    else
                        $nameZIP = $this->zipEmail(storage_path("app/public/{$this->company->identification_number}/{$this->filename}.xml"), storage_path("app/public/{$this->company->identification_number}/FES-{$this->invoice[0]->prefix}{$this->invoice[0]->number}.pdf"));
                    return $this->view('mails.mail')->subject("Emitido por {$this->company->identification_number} - {$this->company->user->name} - Comprobante N⁰ {$this->invoice[0]->prefix}{$this->invoice[0]->number} Tipo Documento: {$this->invoice[0]->type_document->code}")
                                                    ->from(config('mail.username'))
                                                    ->attach($nameZIP);
                }
            }
    }
}
